<?php

namespace Codeacademy\Products\Controller;

use \Codeacademy\Framework\Helper\FormBuilder;
use \Codeacademy\Framework\Helper\Request;
use Codeacademy\Framework\Helper\SqlBuilder;
use \Codeacademy\Products\Model\Product;
use \Codeacademy\Framework\Helper\Url;
use \Codeacademy\Framework\Core\Controller;


class Index extends Controller
{
    private $request;

    public function __construct()
    {
        $this->request= new Request();
        parent::__construct('Codeacademy/products');
    }


    public function index()
    {
        //$product = new Product();
       // $product->load(2);
        //print_r($product);
    }

    public function editProd(){

        $db = new SqlBuilder;
        $allItems = $db->select()->from('products')->get();
        $result = $allItems;

        $data['title'] = 'Edit Product';
        $data['form'] = '';

        $this->render('form/create', $data);
    ?>
    <table class="table table-striped">
        <thead class="table-info">
        <tr>
            <th>Product Name</th>
            <th>Description</th>
            <th>Sku</th>
            <th>Price</th>
            <th>Special Price</th>
            <th>Cost</th>
            <th>Quantity</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($result as $key => $product) : ?>
            <tr>
                <td><?= $product['name'] ?></td>
                <td><?= $product['description'] ?></td>
                <td><?= $product['sku'] ?></td>
                <td><?= $product['price'] ?></td>
                <td><?= $product['special_price'] ?></td>
                <td><?= $product['cost'] ?></td>
                <td><?= $product['qty'] ?></td>
                <td>
                    <?php
                    $formHelper = new FormBuilder('POST', Url::getUrl('products/edit/') . $product['id'], 'form', 'product-form');
                    $formHelper->button('ok', 'Edit','btn btn-info mb-3');
                    echo $formHelper->get();
                    ?>
                </td>
                <td>
                    <?php
                    $formHelper = new FormBuilder('POST', Url::getUrl('products/delete/') . $product['id'], 'form', 'product-form');
                    $formHelper->button('ok', 'Delete','btn btn-danger mb-3');
                    echo $formHelper->get();
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php

    }

    public function edit($id)
    {

        $id = (int) $id;
        $product = new Product();
        $product->load('id',$id);


        $formHelper = new FormBuilder('POST', Url::getUrl('products/update'), 'form', 'product-form');
        $formHelper->input('text','name', 'form-control', 'product-name', 'Product Name', $product->GetName());
        $formHelper->input('text','description', 'form-control', 'product-description', 'Product Description', $product->GetDesc());
        $formHelper->input('text','sku', 'form-control', 'product-sku', 'Product Sku', $product->GetSku());
        $formHelper->input('text','price', 'form-control', 'product-price', 'Product Price', $product->GetPrice());
        $formHelper->input('text','special_price', 'form-control', 'product-special-price', 'Product Special Price', $product->GetSpecialPrice());
        $formHelper->input('text','cost', 'form-control', 'product-cost', 'Product Cost', $product->GetCost());
        $formHelper->input('number','qty', 'form-control', 'product-qty', 'qty', $product->GetQty());
        $formHelper->input('hidden','id', 'form-control', 'product-id', 'id',$id);

        $formHelper->button('ok', 'Edit','btn btn-info mb-3');
        $data['title'] = 'Edit Product';
        $data['form'] = $formHelper->get();

        $this->render('form/create', $data);

    }

    public function update()
    {
        $id = $this->request->getPost('id');
        $name  = $this->request->getPost('name');
        $description  = $this->request->getPost('description');
        // sku is uniq
        $sku  = $this->request->getPost('sku');
        $price  = $this->request->getPost('price');
        $special_price  = $this->request->getPost('special_price');
        $cost  = $this->request->getPost('cost');
        $qty  = $this->request->getPost('qty');
        $product = new Product();
        $product->load('id',$id);

        $product->setName($name);
        $product->setDesc($description);
        $product->setSku($sku);
        $product->setPrice($price);
        $product->setSpecialPrice($special_price);
        $product->setCost($cost);
        $product->setQty($qty);
        $product->save();

    }

    public function create()
    {
        //$this->render('form/create');
        $formHelper = new FormBuilder('POST', Url::getUrl('products/store'), 'form', 'product-form');
        $formHelper->input('text','name', 'form-control', 'product-name', 'Product Name');
        $formHelper->input('text','description', 'form-control', 'product-description', 'Product Description');
        $formHelper->input('text','sku', 'form-control', 'product-sku', 'Product Sku');
        $formHelper->input('text','price', 'form-control', 'product-price', 'Product Price');
        $formHelper->input('text','special_price', 'form-control', 'product-special-price', 'Product Special Price');
        $formHelper->input('text','cost', 'form-control', 'product-cost', 'Product Cost');
        $formHelper->input('number','qty', 'form-control', 'product-qty', 'qty');
        $formHelper->button('ok', 'Create','btn btn-info mb-3');
        $data['title'] = 'Add New Product';
        $data['form'] = $formHelper->get();

        $this->render('form/create', $data);
        //echo $formHelper->get();
        // WEB temnpalte + forma;
    }
// POST requestus
    public function store()
    {
        $name  = $this->request->getPost('name');
        $description  = $this->request->getPost('description');
        // sku is uniq
        $sku  = $this->request->getPost('sku');
        $price  = $this->request->getPost('price');
        $special_price  = $this->request->getPost('special_price');
        $cost  = $this->request->getPost('cost');
        $qty  = $this->request->getPost('qty');
        $product = new Product();
        $product->setName($name);
        $product->setDesc($description);
        $product->setSku($sku);
        $product->setPrice($price);
        $product->setSpecialPrice($special_price);
        $product->setCost($cost);
        $product->setQty($qty);
        $product->save();

    }



    public function delete($id)
    {
        $id = (int) $id;
        $product = new Product();
        $product->delete($id);

    }


    public function import(){
        $url = 'https://manopora.lt/ca/spc.xml';
        $path = PROJECT_ROOT_DIR.'var/import/products.csv';
        $fileName = 'products'.date('d-m-Y').'.xml';
        $file = $path.$fileName;
        $fp=fopen($file,'w+');

        $curlHandler = curl_init($url);
        curl_setopt($curlHandler, CURLOPT_FILE, $fp);
        curl_exec($curlHandler);
        echo '<pre>';

        $xml = simplexml_load_file($file, 'SimpleXMLElement', LIBXML_NOCDATA);
        foreach ($xml->produkt as $product)
        {
            $sku = (string)$product->kzs;
            if ($this->checkSkuUniq($sku)){
                continue;
            }
            $name = (string)$product->nazwa;
            $qty = (int)$product->status;
            $price = (float)$product->cena_zewnetrzna;
            $description = (string)$product->dlugi_opis;
            $cost = (float)$product->cena_zewnetrzna_hurt;

            $newProduct = new Product();
            $newProduct->setSku($sku);
            $newProduct->setName($name);
            $newProduct->setQty($qty);
            $newProduct->setPrice($price);
            $newProduct->setDesc($description);
            $newProduct->setCost($cost);

            $newProduct->save();

        }

    }

    public function stockUpdate()
    {
        //get csv
        $url = 'https://manopora.lt/ca/stocks.csv';
        $path = PROJECT_ROOT_DIR . 'var/import/';
        $fileName = 'productStocks' . date('d-m-Y') . '.csv';
        $file = $path . $fileName;
        $fp = fopen($file, 'r');

        if ($fp !== FALSE) {
            $firstline = true;
            while (($data = fgetcsv($fp, 1000, ",")) !== FALSE) {
                if ($firstline === true) {
                    $firstline = false;
                    continue;
                } else {
                    $sku = $data[0];
                    $qty = $data[1];
                    $is_in_stock = $data[2];

                    if (!$this->checkSkuUniq($sku)){
                        continue;
                    } else {

                    $product = new Product();
                    $product->load('sku', $sku);
                    $product->setQty($qty);
                    $product->save(1);
                    }
                }
            }
        }
    }



    public function checkSkuUniq($sku){
        $db = new SqlBuilder;
        $allSku = $db->select('sku')->from('products')->get();

        $allSku = array_merge_recursive(...array_values($allSku))["sku"];

        if(in_array($sku, $allSku) || $sku === "" || $sku === Null) {
            echo "Product with SKU: " . $sku . " already exists<br>";
            return true;
        } else {
            echo "Product with SKU: " . $sku . " is unique or SKU input was empty.<br>";
            return false;
        }
    }

}