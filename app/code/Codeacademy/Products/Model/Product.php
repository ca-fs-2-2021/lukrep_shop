<?php

namespace Codeacademy\Products\Model;

use \Codeacademy\Framework\Helper\SqlBuilder;

class Product
{
    private $id;
    private $name;
    private $description;
    private $sku;
    private $price;
    private $special_price;
    private $cost;
    private $qty;

    public function __construct($id = '')
    {
        if ($id !== '') {
            $this->load($id);
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDesc()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDesc($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku): void
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getSpecialPrice()
    {
        return $this->special_price;
    }

    /**
     * @param mixed $special_price
     */
    public function setSpecialPrice($special_price): void
    {
        $this->special_price = $special_price;
    }

    /**
     * @return mixed
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param mixed $cost
     */
    public function setCost($cost): void
    {
        $this->cost = $cost;
    }

    /**
     * @return mixed
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param mixed $qty
     */
    public function setQty($qty): void
    {
        $this->qty = $qty;
    }

    public function load($field, $value, $selection = '*')
    {
        $db = new SqlBuilder();
       // print_r($db->select($selection)->from('products')->where($field, $value)->getOne());
       // die();
        $product = $db->select($selection)->from('products')->where($field, $value)->getOne();


        $this->id = $product['id'] ?? '';
        $this->name = $product['name'] ?? '';
        $this->description = $product['description'] ?? '';
        $this->sku = $product['sku'] ?? '';
        $this->price = $product['price'] ?? '';
        $this->special_price = $product['special_price'] ?? '';
        $this->cost = $product['cost'] ?? '';
        $this->qty = $product['qty'] ?? '';

        return $this;
    }

    public function save($stockUpdate = '')
    {
        if ($stockUpdate==1) {

            $this->updateStock();
        } elseif ($this->id) {
            $this->update();
        } else {
            $this->create();
        }

    }

    private function update()
    {
        $db = new SqlBuilder();

        $product = [
            //'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'sku' => $this->sku,
            'price' => $this->price,
            'special_price' => $this->special_price,
            'cost' => $this->cost,
            'qty' => $this->qty,
        ];

        $db->update('products')->set($product)->where('id', $this->id)->exec();
    }

    private function updateStock()
    {
        $db = new SqlBuilder();

        $product = [
            'qty' => $this->qty,
        ];

        $db->update('products')->set($product)->where('sku', $this->sku)->exec();
    }

    private function create()
    {
        $product = [
            'name' => $this->name,
            'description' => $this->description,
            'sku' => $this->sku,
            'price' => $this->price,
            'special_price' => $this->special_price,
            'cost' => $this->cost,
            'qty' => $this->qty,
        ];

        $db = new SqlBuilder();
        $db->insert('products')->values($product)->exec();
    }

    public function delete($id)
    {

        $db = new SqlBuilder();
        $db->delete('products')->where('id', $id)->exec();

    }


    public function checkSkuUnix()
    {
        //
    }

}


