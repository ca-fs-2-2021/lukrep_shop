<?php


namespace Codeacademy\Products\Model\Collection;

use \Codeacademy\Framework\Helper\SqlBuilder;
use \Codeacademy\Products\Model\Product;

use \Codeacademy\Framework\Helper\Debug as D;
// D::dd($element); ^^^^^^^^^


class Products
{

    private $collection = [];

    public function __construct()
    {
        $this->initCollection();
        return $this;
    }


    //move to sqlbuilder
   //private function cleanResults($result){
   //    $cleanArray = [];
   //    foreach ($result as $element){
   //        foreach ($element as $line){
   //            $cleanArray[] = $line;
   //        }
   //    }
   //    return $cleanArray;
   //}


    public function addCategoryFilter($id)
    {

        $db = new SqlBuilder();
        $productIds = $db->select('product_id')->from('category_related_products')->where('category_id', $id)->get();
        $productIds = $db->cleanResults($productIds);
        echo "<pre>";
        //print_r($productIds);
        foreach ($this->collection as $product) {

            if(!in_array($product->getId(), $productIds)){
                unset($this->collection[$product->getId()]);
            }
        }
    }

    public function addFilter($filter, $value, $operator)
    {

    }

    public function getCollection()
    {
    return $this->collection;
    }

    public function initCollection()
    {
        $db = new SqlBuilder();
        $productsIds = $db->select('id')->from('products')->get(); //->where() <- jei neveiks

        foreach ($productsIds as $element){
            $product = new Product();
            $this->collection[$element['id']] = $product->load('id',$element['id']);
        }
    }
}