<?php

namespace Codeacademy\Framework\Helper;

class FormBuilder
{
    private $form = '';


    public function __construct($method, $action, $class = '', $id = '')
    {
        $this->form .= "<form method='$method' action='$action' class='$class' id='$id'>";
        return $this;
    }

    public function input($type, $name, $class = '', $id = '', $placeholder = '', $value = '', $label = '', $wrap = '')
    {
        if ($label !== '' && $id !== '') {
            $this->form .= "<label for='$id'>$label</label>";
        }
        $this->form .= "<input type='$type' name='$name' class='$class' id='$id' placeholder='$placeholder' value='$value'><br>";
        return $this;
    }

    public function select($name, $options, $label)
    {
        if ($label !== '') {
            $this->form .= "<label for ='$name'>$label</label>";
        }
        $this->form .= "<select name='$name'>";
        foreach ($options as $id => $option) {
            $this->form .= "<option value='$id'>$option</option>";
        }
        $this->form .= "</select>";
        return $this;
    }

    //'description','describe your self', 'textarea-input'
    public function textarea($name, $placeholder, $class)
    {
        $this->form .= "<textarea name='$name' class='$class' placeholder='$placeholder'></textarea>";
        return $this;
    }


    public function button($name, $text, $class = '')
    {
        $this->form .= "<button name='$name' class='$class'>$text</button><br>";
        return $this;
    }

    public function get()
    {
        return $this->form . '</form>';
    }


}