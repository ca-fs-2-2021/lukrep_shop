<?php

namespace Codeacademy\Framework\Core;


class Controller
{
    protected $moduleName;

    public function __construct($moduleName)
    {
        $this->moduleName = $moduleName;
    }


    public function render($template, $data){
       include_once PROJECT_ROOT_DIR . 'app/code/Codeacademy/Framework/view/header.php';
       include_once PROJECT_ROOT_DIR . 'app/code/' . $this->moduleName . '/view/'. $template . '.php';
       include_once PROJECT_ROOT_DIR . 'app/code/Codeacademy/Framework/view/foot.php';
    }
}