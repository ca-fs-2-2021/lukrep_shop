<?php

namespace Codeacademy\Home\Controller;

use \Codeacademy\Framework\Core\Controller;
use Codeacademy\Framework\Helper\Request;

class Index extends Controller
{
    private $request;

    public function __construct()
    {
        $this->request= new Request();
        parent::__construct('Codeacademy/Home');
    }

    public function index()
    {
        $data['title'] = 'Welcome';
        $this->render('homepage', $data);
    }
}