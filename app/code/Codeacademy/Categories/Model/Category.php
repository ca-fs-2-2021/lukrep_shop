<?php

namespace Codeacademy\Categories\Model;

use \Codeacademy\Framework\Helper\SqlBuilder;

class Category
{
    private $id;
    private $name;
    private $description;
    private $parent_id;

    public function __construct($id = '')
    {
        if ($id !== '') {
            $this->load($id);
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDesc()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDesc($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * @param mixed $parent_id
     */
    public function setParentId($parent_id): void
    {
        $this->parent_id = $parent_id;
    }


    public function load($field, $value, $selection = '*')
    {
        $db = new SqlBuilder();
        // print_r($db->select($selection)->from('products')->where($field, $value)->getOne());
        // die();
        $category = $db->select($selection)->from('categories')->where($field, $value)->getOne();


        $this->id = $category['id'] ?? '';
        $this->name = $category['name'] ?? '';
        $this->description = $category['description'] ?? '';
        $this->parent_id = $category['parent_id'] ?? '';

        return $this;
    }

    public function save()
    {
        if ($this->id) {
            $this->update();
        } else {
            $this->create();
        }

    }

    private function update()
    {
        $db = new SqlBuilder();

        $category = [
            //'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'parent_id' => $this->parent_id,
        ];

        $db->update('categories')->set($category)->where('id', $this->id)->exec();
    }

    private function create()
    {
        $category = [
            'name' => $this->name,
            'description' => $this->description,
            'parent_id' => $this->parent_id,
        ];

        $db = new SqlBuilder();
        $db->insert('categories')->values($category)->exec();
    }

    public function delete($id)
    {

        $db = new SqlBuilder();
        $db->delete('categories')->where('id', $id)->exec();

    }


    public function checkSkuUnix()
    {
        //
    }

}


