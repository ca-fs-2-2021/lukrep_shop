<?php

namespace Codeacademy\Categories\Controller;

use \Codeacademy\Framework\Helper\FormBuilder;
use \Codeacademy\Framework\Helper\Request;
use \Codeacademy\Categories\Model\Category;
use \Codeacademy\Framework\Helper\Url;
use \Codeacademy\Products\Model\Collection\Products;
use \Codeacademy\Framework\Core\Controller;


class Index extends Controller
{
    private $request;

    public function __construct()
    {
        $this->request= new Request();
        parent::__construct('Codeacademy/products');
    }


    public function index()
    {
       // $category = new Product();
        // $product->load(2);
        //print_r($product);
    }

    public function show($id)
    {
        $productsModel = new Products();
        $productsModel->addCategoryFilter($id);
        $products = $productsModel->getCollection();

        print_r($products);
    }

    public function edit($id)
    {

        $id = (int) $id;
        $category = new Category();
        $category->load('id',$id);

        $formHelper = new FormBuilder('POST', Url::getUrl('categories/update'), 'form', 'category-form');
        $formHelper->input('text','name', 'form-control', 'category-name', 'Category Name', $category->GetName());
        $formHelper->input('text','description', 'form-control', 'category-description', 'Category Description', $category->GetDesc());
        $formHelper->input('number','parent_id', 'form-control', 'category-id', 'Category Parent Id', $category->GetParentId());

        $formHelper->input('hidden','id', 'form-control', 'category-id', 'id',$id);

        $formHelper->button('ok', 'Edit','btn btn-info mb-3');
        $data['title'] = 'Edit Category';
        $data['form'] = $formHelper->get();

        $this->render('form/create', $data);

    }

    public function update()
    {
        $id = $this->request->getPost('id');
        $name  = $this->request->getPost('name');
        $description  = $this->request->getPost('description');
        // sku is uniq
        $parent_id  = $this->request->getPost('parent_id');
        $category = new Category();
        $category->load('id',$id);

        $category->setName($name);
        $category->setDesc($description);
        $category->setParentId($parent_id);
        $category->save();

    }

    public function create()
    {
        $formHelper = new FormBuilder('POST', Url::getUrl('categories/store'), 'form', 'category-form');
        $formHelper->input('text','name', 'form-control', 'category-name', 'Category Name');
        $formHelper->input('text','description', 'form-control', 'category-description', 'Category Description');
        $formHelper->input('text','parent_id', 'form-control', 'category-sku', 'Category Parent Id');


        $formHelper->button('ok', 'Create','btn btn-info mb-3');
        $data['title'] = 'Add New Category';
        $data['form'] = $formHelper->get();

        $this->render('form/create', $data);
        // WEB temnpalte + forma;
    }
// POST requestus
    public function store()
    {
        $name  = $this->request->getPost('name');
        $description  = $this->request->getPost('description');
        $parent_id  = $this->request->getPost('parent_id');
        $category = new Category();
        $category->setName($name);
        $category->setDesc($description);
        $category->setParentId($parent_id);

        $category->save();

    }



    public function delete($id)
    {
        $id = (int) $id;
        $category = new Category();
        $category->delete($id);

    }

}